﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ontology_Module;
using OntologyViewModels.DataAdapter;

namespace OntologyViewModlesWinFormsTester
{
    public partial class WinFormsTester : Form
    {
        private Globals globals;
        private ViewModelWinFormsOItemList viewModelOitemList;
        private bool advancedFilter = false;

        int rowIndexFromMouseDown;
        DataGridViewRow rw;

        private delegate void PropertyChanged(object sender, PropertyChangedEventArgs e);

        public WinFormsTester()
        {
            InitializeComponent();
            globals = new Globals();

            Initialize();
        }

        private void Initialize()
        {
            viewModelOitemList = new ViewModelWinFormsOItemList(globals);
            viewModelOitemList.PropertyChanged += ViewModelOitemList_PropertyChanged;

            toolStripLabel_List.Image = LoadStateToImageConverter.ConvertListFilterState(viewModelOitemList.ListLoadState);
            toolStripLabel_AdvancedFilter.Image = LoadStateToImageConverter.ConvertAdvancedFilterState(viewModelOitemList.ListLoadState);

            //viewModelOitemList.WinForms_Initialize_ClassAttListForGridView(new clsClassAtt { ID_Class = "bee3fac561d5463092dda4e1ed0bc670" }, dataGridView_ItemList, false);
            //viewModelOitemList.WinForms_Initialize_NoteListForGridView(ListType.AttributeTypes, null, dataGridView_ItemList, false);
            viewModelOitemList.WinForms_Initialize_NoteListForGridView(ListType.Instances, new clsOntologyItem { GUID_Parent = "c8aa7b6a8336491899adf2bf6ea89c34" }, dataGridView_ItemList, false);

            toolStripButton_Up.Enabled = viewModelOitemList.IsEnabled_MoveUp;
            toolStripButton_Down.Enabled = viewModelOitemList.IsEnabled_MoveDown;
            toolStripButton_Reorder.Enabled = viewModelOitemList.IsEnabled_Reorder;
        }

        private void ViewModelOitemList_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var deleg = new PropertyChanged(ViewModelOitemList_PropertyChanged);
                this.Invoke(deleg, sender, e);
            }
            else
            {
                if (e.PropertyName == PropertyName.OItemList_ViewModel_ListLoadState)
                {
                    toolStripLabel_List.Image = LoadStateToImageConverter.ConvertListFilterState(viewModelOitemList.ListLoadState);
                    toolStripLabel_AdvancedFilter.Image = LoadStateToImageConverter.ConvertAdvancedFilterState(viewModelOitemList.ListLoadState);
                }
                else if (e.PropertyName == PropertyName.OItemList_ListAdapter_Result_ListLoader
                    && viewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.ListLoadLoaded)
                    && !viewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.ListLoadPending)
                    && !viewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterLoaded)
                    && !viewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterPending))
                {
                    //advancedFilter = true;
                    //viewModelOitemList.Initialize_AdvancedFilter(advancedFilter_Direction: globals.Direction_LeftRight, advancedFilter_Class: new clsOntologyItem { GUID = "6f9bf5b4c942465a8c788a7ac8224112" });
                    
                }
                else if (e.PropertyName == PropertyName.OItemList_CountItems)
                {
                    toolStripLabel_Count.Text = viewModelOitemList.CountItems.ToString();
                }
                else if (e.PropertyName == PropertyName.OItemList_IdSelected)
                {
                    toolStripTextBox_Id.Text = viewModelOitemList.IdSelected;
                }
                else if (e.PropertyName == PropertyName.OItemList_AddAllowed)
                {
                    toolStripButton_Add.Enabled = viewModelOitemList.AddAllowed;
                    
                }
                else if (e.PropertyName == PropertyName.OItemList_DelAllowed)
                {
                    toolStripButton_Del.Enabled = viewModelOitemList.DelAllowed;
                }
            }
        }

        private void dataGridView_ItemList_SelectionChanged(object sender, EventArgs e)
        {
            toolStripButton_Up.Enabled = viewModelOitemList.IsEnabled_MoveUp;
            toolStripButton_Down.Enabled = viewModelOitemList.IsEnabled_MoveDown;
            toolStripButton_Reorder.Enabled = viewModelOitemList.IsEnabled_Reorder;
        }

        private void toolStripButton_Up_Click(object sender, EventArgs e)
        {
            var itemToMove = dataGridView_ItemList.SelectedRows[0].DataBoundItem;
            var itemMoveTo = dataGridView_ItemList.Rows[dataGridView_ItemList.SelectedRows[0].Index - 1].DataBoundItem;

            viewModelOitemList.MoveItemInList(itemToMove, itemMoveTo, true);

        }

        private void toolStripButton_Down_Click(object sender, EventArgs e)
        {
            var itemToMove = dataGridView_ItemList.SelectedRows[0].DataBoundItem;
            var itemMoveTo = dataGridView_ItemList.Rows[dataGridView_ItemList.SelectedRows[0].Index + 1].DataBoundItem;

            viewModelOitemList.MoveItemInList(itemToMove, itemMoveTo, false);
        }

        private void toolStripButton_Reorder_Click(object sender, EventArgs e)
        {
            var item = dataGridView_ItemList.SelectedRows[0].DataBoundItem;
            var orderId = viewModelOitemList.GetOrderIdOfItem(item);
            var dlgAttributeLng = new dlg_Attribute_Long("OrderId", globals, orderId);
            if (dlgAttributeLng.DialogResult == DialogResult.OK)
            {
                var newOrderId = dlgAttributeLng.Value;


            }
        }

        private void toolStripButton_Add_Click(object sender, EventArgs e)
        {
            var addFormItem = viewModelOitemList.GetAddFormItemType();
            
            switch(addFormItem.AddFormType)
            {
                case AddFormType.ClassAttribute:
                    var result1 = AddClassAttribute(addFormItem);
                    break;
                case AddFormType.ClassClass_Conscious:
                    var result2 = AddClassClass_Conscious(addFormItem);


                    break;

                case AddFormType.ClassClass_Subconscious:
                    var result3 = AddClassClass_SubConscious(addFormItem);

                    break;

                case AddFormType.ClassOther:
                    var result4 = AddClassOther(addFormItem);
                    break;

                case AddFormType.AttributeTypes:
                    var result5 = AddAttributeTypes(addFormItem);
                    break;

                case AddFormType.RelationTypes:
                    var result6 = AddRelationTypes(addFormItem);
                    break;

                case AddFormType.Classes:
                    var result7 = AddClasses(addFormItem);
                    break;

                case AddFormType.Instances:
                    var result8 = AddInstances(addFormItem);
                    break;

            }
            
        }

        private clsOntologyItem AddAttributeTypes(AddItemAdapter addAdapter)
        {
            var result = CheckSimpleItemAdd(addAdapter);
            if (result.GUID == globals.LState_Success.GUID)
            {
                result = addAdapter.Add_AtirbuteTypes();
            }
            else if (result.GUID == globals.LState_Relation.GUID)
            {
                MessageBox.Show(this, "An item with this name exists!", "Exists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show(this, "An Error occured!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;
        }

        private clsOntologyItem AddRelationTypes(AddItemAdapter addAdapter)
        {
            var result = CheckSimpleItemAdd(addAdapter);
            if (result.GUID == globals.LState_Success.GUID)
            {
                result = addAdapter.Add_RelationTypes();
            }
            else if (result.GUID == globals.LState_Relation.GUID)
            {
                MessageBox.Show(this, "An item with this name exists!", "Exists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show(this, "An Error occured!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return result;
        }

        private clsOntologyItem AddClasses(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemNote.GUID, globals.Type_Class);
            if (oItemClass != null)
            {
                result = CheckSimpleItemAdd(addAdapter);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result = addAdapter.Add_Classes(oItemClass);
                }
                else if (result.GUID == globals.LState_Relation.GUID)
                {
                    MessageBox.Show(this, "An item with this name exists!", "Exists", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show(this, "An Error occured!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

            }
            else
            {
                result = globals.LState_Error.Clone();
            }
            

            return result;
        }

        private clsOntologyItem AddInstances(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemNote.GUID, globals.Type_Class);
            if (oItemClass != null)
            {
                addAdapter.EntryItem = oItemClass;
                result = CheckSimpleItemAdd(addAdapter);
                if (result.GUID == globals.LState_Success.GUID)
                {
                    result = addAdapter.Add_Instances(oItemClass);
                }
                else if (result.GUID == globals.LState_Relation.GUID)
                {
                    if (MessageBox.Show(this,"There are doublicates. Do you want to create the list?","Doublicates",MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        result = addAdapter.Add_Instances(oItemClass);
                    }
                }
            }
            else
            {
                result = globals.LState_Error.Clone();
            }


            return result;
        }

        private clsOntologyItem CheckSimpleItemAdd(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var frm_Name = new frm_Name(addAdapter.ItemTypeFirst, globals, isListPossible:true);
            frm_Name.ShowDialog(this);
            addAdapter.ResultAttributes = new List<ResultAttribute>();
            if (frm_Name.DialogResult == DialogResult.OK)
            {
                var names = new List<string>();
                if (frm_Name.List)
                {
                    names.AddRange(frm_Name.Values);
                }
                else
                {
                    names.Add(frm_Name.Value1);
                }

                addAdapter.ResultAttributes = names.Select(nameItem => new ResultAttribute { ResultStr = nameItem }).ToList();
                
                if (addAdapter.CheckOk())
                {
                    
                    result = globals.LState_Success.Clone();
                }
                else
                {
                    if (addAdapter.NamesError == NameAddError.Exists)
                    {
                        result = globals.LState_Relation.Clone();
                     
                    }
                    else
                    {
                        result = globals.LState_Error.Clone();
                     
                    }
                    
                }
            }

            return result;
        }

        private clsOntologyItem AddClassOther(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Left, globals.Type_Class);
            var frmMain = new frmMain(globals, addAdapter.ItemTypeFirst, Caption: "Add RelationType");
            frmMain.ShowDialog(this);
            if (oItemClass != null && frmMain.DialogResult == DialogResult.OK)
            {

                addAdapter.TypeApplied1 = frmMain.Type_Applied;
                addAdapter.ResultItems_OntologyEditor1 = frmMain.OList_Simple;
                if (addAdapter.CheckOk())
                {
                    result = addAdapter.Add_ClassOther(oItemClass);
                }
                else
                {
                    result = globals.LState_Error.Clone();
                }
            }

            return result;

        }

        private clsOntologyItem AddClassAttribute(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemClassAtt.ID_Class, globals.Type_Class);
            var frmMain = new frmMain(globals, addAdapter.ItemTypeFirst, Caption: "Add AttributeType");
            frmMain.ShowDialog(this);
            if (oItemClass != null && frmMain.DialogResult == DialogResult.OK)
            {
                
                addAdapter.TypeApplied1 = frmMain.Type_Applied;
                addAdapter.ResultItems_OntologyEditor1 = frmMain.OList_Simple;
                if (oItemClass != null && addAdapter.CheckOk())
                {
                    result = addAdapter.Add_ClassAttributes(oItemClass);
                }
                else
                {
                    result = globals.LState_Error.Clone();
                }
            }

            return result;

        }

        private clsOntologyItem AddClassClass_Conscious(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Left, globals.Type_Class);
            var frmMain_Classes = new frmMain(globals, addAdapter.ItemTypeFirst);
            frmMain_Classes.ShowDialog(this);
            if (frmMain_Classes.DialogResult == DialogResult.OK)
            {
                addAdapter.TypeApplied1 = frmMain_Classes.Type_Applied;
                addAdapter.ResultItems_OntologyEditor1 = frmMain_Classes.OList_Simple;
                if (addAdapter.CheckOk(true))
                {
                    var frmMain_RelationTypes = new frmMain(globals, addAdapter.ItemTypeSecond);
                    frmMain_RelationTypes.ShowDialog(this);
                    if (frmMain_RelationTypes.DialogResult == DialogResult.OK)
                    {
                        addAdapter.TypeApplied2 = frmMain_RelationTypes.Type_Applied;
                        addAdapter.ResultItems_OntologyEditor2 = frmMain_RelationTypes.OList_Simple;

                        if (oItemClass != null && addAdapter.CheckOk(false))
                        {
                            result = addAdapter.Add_ClassClass_Conscious(oItemClass);
                        }
                        else
                        {
                            result = globals.LState_Error.Clone();
                        }
                    }
                }
                else
                {
                    result = globals.LState_Error.Clone();
                }

            }

            return result;
        }

        private clsOntologyItem AddClassClass_SubConscious(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemClass = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Right, globals.Type_Class);
            var frmMain_Classes = new frmMain(globals, addAdapter.ItemTypeFirst);
            frmMain_Classes.ShowDialog(this);
            if (frmMain_Classes.DialogResult == DialogResult.OK)
            {
                addAdapter.TypeApplied1 = frmMain_Classes.Type_Applied;
                addAdapter.ResultItems_OntologyEditor1 = frmMain_Classes.OList_Simple;

                if (addAdapter.CheckOk(true))
                {
                    
                    var frmMain_RelationTypes = new frmMain(globals, addAdapter.ItemTypeSecond);
                    frmMain_RelationTypes.ShowDialog(this);
                    if (frmMain_RelationTypes.DialogResult == DialogResult.OK)
                    {
                        addAdapter.TypeApplied2 = frmMain_RelationTypes.Type_Applied;
                        addAdapter.ResultItems_OntologyEditor2 = frmMain_RelationTypes.OList_Simple;

                        if (oItemClass != null && addAdapter.CheckOk(false))
                        {
                            result = addAdapter.Add_ClassClass_Subconscious(oItemClass);
                        }
                        else
                        {
                            result = globals.LState_Error.Clone();
                        }
                    }
                }
                else
                {
                    result = globals.LState_Error.Clone();
                }
                
            }

            return result;
        }

        private clsOntologyItem AddInstanceInstance_Conscious(AddItemAdapter addAdapter)
        {
            var result = globals.LState_Nothing.Clone();
            var oItemInstance = viewModelOitemList.ListAdapter.OItem(viewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Left, globals.Type_Class);
            var frmMain_Classes = new frmMain(globals, addAdapter.ItemTypeFirst);
            frmMain_Classes.ShowDialog(this);
            if (frmMain_Classes.DialogResult == DialogResult.OK)
            {
                addAdapter.TypeApplied1 = frmMain_Classes.Type_Applied;
                addAdapter.ResultItems_OntologyEditor1 = frmMain_Classes.OList_Simple;
                if (addAdapter.CheckOk(true))
                {
                    var frmMain_RelationTypes = new frmMain(globals, addAdapter.ItemTypeSecond);
                    frmMain_RelationTypes.ShowDialog(this);
                    if (frmMain_RelationTypes.DialogResult == DialogResult.OK)
                    {
                        addAdapter.TypeApplied2 = frmMain_RelationTypes.Type_Applied;
                        addAdapter.ResultItems_OntologyEditor2 = frmMain_RelationTypes.OList_Simple;

                        if (oItemClass != null && addAdapter.CheckOk(false))
                        {
                            result = addAdapter.Add_ClassClass_Conscious(oItemClass);
                        }
                        else
                        {
                            result = globals.LState_Error.Clone();
                        }
                    }
                }
                else
                {
                    result = globals.LState_Error.Clone();
                }

            }

            return result;
        }
    }
}
