﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OntologyViewModels.OItemList;
using OntologyViewModels.DataAdapter;

namespace OntologyViewModlesWinFormsTester
{
    public static class LoadStateToImageConverter
    {
        public static Image ConvertListFilterState(ListLoadStateItem listLoadState )
        {
            
            if (listLoadState.HasFlag(ListLoadStateItem.ListLoadLoaded))
            {
                return Properties.Resources.ListLoadedImage;
            }
            return Properties.Resources.ListLoadingPending;
        }

        public static Image ConvertAdvancedFilterState(ListLoadStateItem listLoadState)
        {
            if (listLoadState.HasFlag(ListLoadStateItem.AdvancedFilterLoaded))
            {
                return Properties.Resources.AdvancedFilterLoaded;
            }
            return Properties.Resources.AdvancedFilterPending;
        }
    }
}
